import express from 'express'
import bodyParser from 'body-parser'
import dotenv from 'dotenv';

dotenv.config();


import { router } from './router/router'


const app = express()


// MARK: - middlewares
app.use(bodyParser.urlencoded({ extended: true }))


app.use(router)



const PORT = process.env.PORT || 80
app.listen(PORT, () => console.log(`Started on ${PORT}`))
