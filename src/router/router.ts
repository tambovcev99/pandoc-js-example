import { Router, Request, Response, NextFunction } from 'express'
import {ConvertFileSync} from 'pandocjs'

const router = Router()

/* Pandoc logic */
// pandoc.ConvertFileSync('demo.docx', 'docx', 'document.md', 'markdown');
// pandoc.ConvertFileSync('demo.docx', 'docx', 'document.txt', 'rst');


router.get('/', (req: Request, res: Response) => {
  console.log('GET')
  res.send({"hey": "world"})
  ConvertFileSync('demo.docx', 'docx', 'document.txt', 'rst');
})

export { router }
