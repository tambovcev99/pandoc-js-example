# FROM alpine
FROM alpine

WORKDIR /home/app


# alpine
RUN apk add --update nodejs nodejs-npm

COPY package*.json ./



# Install
RUN npm install

# Bundle app source
COPY . .

EXPOSE 80


# Build
RUN npm build

CMD ["npm start"]


